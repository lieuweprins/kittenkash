var Expense = Backbone.Model.extend({
	idAttribute: "id",
	initialize: function() { 

	},
	defaults: {
		"title":  "",
		"amount" : 0,
		"description" : "",
		"paid_at" : "0000-00-00",
		"category_id": 0,
		"fixed_charge": 0
	}
});