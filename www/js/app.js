	function showcategory(id){
			window.current_category_id=id;
			$("#category-view").html("");
			categoryview=new CategoryView({ model: categories.get(id), collection: expenses });
			
			window.location.href="#category-view";
		}
	
		$(function(){
			if(window.location.hash!==""){
				window.location.href="";
			}
			
			$('#calendar').fullCalendar({  
			editable:true,
			eventDrop: function(event, delta, revertFunc) {

				database.query("UPDATE expenses SET paid_at = ? WHERE id = ?",[event.start.format(),event.id]);

			},
			dayClick: function(date, jsEvent, view) {
				window.current_date=date.toDate();
				$( "#add-expense-dialog" ).popup( "open" ); 
			},
			header: {
				left: 'prev,next',
				center: '',
				right: 'title'
			}}
			);
		});
	
		var categories = new Categories();
		var expenses = new Expenses();
		
		var historyview = new HistoryView({model:expenses});
		var categoriesview = new CategoriesView({model:categories});
		var categoryview = null;
			
		function getform(el){
			var arr=el.serializeArray();
			var values = {};
			for(var i in arr){
				values[arr[i].name]=arr[i].value;
			}
			el[0].reset();
			return values;
		}
				
		//jquery moblie is ready
		$(document).on('pagebeforeshow', '#index-view', function(){ 
			
			window.current_category_id=0;
			
		});
		
		
		$(function(){
			
			var config = {
				// How long Waves effect duration 
				// when it's clicked (in milliseconds)
				duration: 500,

				// Delay showing Waves effect on touch
				// and hide the effect if user scrolls
				// (0 to disable delay) (in milliseconds)
				delay: 200
			};
							
			// Initialise Waves with the config
			Waves.init(config);
			
			$( "body>[data-role='panel']" ).panel().enhanceWithin();
			$( "body>[data-role='popup']" ).popup().enhanceWithin();
			$( "[data-role='header'], [data-role='footer']" ).toolbar();
			
			$(document).on("popupafteropen", "#add-expense-dialog",function( event, ui ) {
				console.log("open event");
				$("#add-expense-dialog input[name=title]").focus();
				$("#add-expense-dialog select[name=category_id]").html("<option disabled selected value='0'>Select a category</option>");
				categories.each(function(category, index) {
					$("#add-expense-dialog select[name=category_id]").append("<option value='" + category.get("id") + "'>" + category.get("name") + "</option>");
				});
				$("#add-expense-dialog select[name=category_id]").val(window.current_category_id);
				$("#add-expense-dialog select[name=category_id]").selectmenu("refresh");
			});  
			
			$(document).on("popupafteropen", "#create-category-dialog",function( event, ui ) {
				$("#create-category-dialog input[name=name]").focus();
			});
			
			$("#create-category-button").click(function(){
				var values = getform($("#create-category-form"));
				database.insert("INSERT INTO categories (name) VALUES (?)",[values.name],function(id){
					categories.add({"id":id, "name":values.name});				
				});
			});
			
			$("#add-expense-button").click(function(){
				
				$("#add-expense-form").find("input[name=paid_at]").val(window.current_date.getFullYear() + "-" + (window.current_date.getMonth()+1) + "-" + window.current_date.getDate() + " 00:00:00");
				var values = getform($("#add-expense-form"));
				values.fixed_charge=parseInt(values.fixed_charge);
				values.amount=values.amount.replace(',','.');
				database.insert("INSERT INTO expenses (amount,title,paid_at,category_id,fixed_charge) VALUES (?,?,?,?,?)",[values.amount,values.title,values.paid_at,values.category_id,values.fixed_charge],function(id){
					//categories.add({"id":id, "name":values.name});				
					values.id=id;
					expenses.add(values);
					$("#calendar").fullCalendar( 'renderEvent', { id: id, allDay: true, "title" : values.title + " (" + values.amount + ")", "start" : moment(values.paid_at) },true)
				});
			});
			
		
			
			var tableArray = [
				
				"CREATE TABLE IF NOT EXISTS expenses (`id` INTEGER PRIMARY KEY AUTOINCREMENT, amount REAL, title TEXT,  paid_at TEXT, category_id INTEGER, fixed_charge INTEGER)",

				"CREATE TABLE IF NOT EXISTS categories (`id` INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, icon TEXT)"
				
			];
		
			database.init(tableArray, function(){		
				database.query("SELECT * FROM categories",[], function(result){
					if(result.rows.length==0){
						database.insert("INSERT INTO categories (name) VALUES (?)",["Default"],function(id){
							categories.add({"id":id, "name":"Default"});				
						});					}
					for(var i=0;i<result.rows.length;i++){
						categories.add(result.rows.item(i));
					}
					database.query("SELECT * FROM expenses",[], function(result){
						var all=[];
						console.warn("loading expenses");
						for(var i=0;i<result.rows.length;i++){
							all.push(result.rows.item(i));
							$("#calendar").fullCalendar( 'renderEvent', { id: result.rows.item(i).id, allDay: true, "title" : result.rows.item(i).title + " (" + result.rows.item(i).amount + ")", "start" : moment(result.rows.item(i).paid_at) },true);
						}
						console.warn("expenses loaded");
						expenses.add(all);
						draw_stats();
					});
				});
				
			});
		});
		
		
		function dateToYmd(date){
			var datestring = date.getFullYear() + "-" + ('0' + (date.getMonth()+1)).slice(-2) + "-" + ('0' + date.getDate()).slice(-2);
			return datestring;
		}
		
		
		function draw_stats(){
		
		var last_month=new Date();
		last_month.setMonth(new Date().getMonth()-1);
		var tomorrow=new Date();
		tomorrow.setDate(new Date()+1);
		
		database.query("SELECT SUM(amount) AS y, `categories`.name AS name FROM expenses JOIN categories ON categories.id = expenses.category_id WHERE paid_at BETWEEN ? AND ? GROUP BY category_id",[dateToYmd(last_month),dateToYmd(tomorrow)],function(result){
	var data=[];
	
for(var i=0;i<result.rows.length;i++){ data.push(result.rows.item(i)); }
    $('#container').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Total expenditures, grouped'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            "data": data
        }]
    });
	
});




		
		}