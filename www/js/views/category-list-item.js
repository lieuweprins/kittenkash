var CategoryListItemView = Backbone.View.extend({
	
	tagName: "li",

  initialize: function() {
	this.template = _.template($('#category-list-item-template').html());
    this.listenTo(this.model, "change", this.render);
  },

  render: function() {
	this.$el.html(this.template(this.model.attributes));
	return this;
  }

});