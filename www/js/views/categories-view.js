var CategoriesView = Backbone.View.extend({

	el: "#categories-view",

  initialize: function() {
	this.template = _.template($('#categories-view-template').html());
    this.listenTo(this.model, "add remove update change", this.render);
	this.render();
  },

  render: function() {
	this.$el.html(this.template({}));//this.model.attributes));
	this.$el.find(".items").html("");
	var last_paid_at=null;
	for(var i = 0; i < this.model.length; ++i) {
			var m=this.model.at(i);
			var m_catview = new CategoryListItemView({model: m});
			this.$el.find(".items").append(m_catview.$el); 
			m_catview.render();
	}
	if(this.$el.find(".items").hasClass("ui-listview")){
		this.$el.find(".items").listview("refresh");
	} else {
		this.$el.find(".items").listview();
	}
	return this;
  }

});

