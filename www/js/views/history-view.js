var HistoryView = Backbone.View.extend({

	el: "#history-view",

  initialize: function() {
	this.template = _.template($('#history-view-template').html());
    this.listenTo(this.model, "add remove update change", this.render);
	this.amount=10;
  },
  
  events: {
		'click a.showmore':'doStuff'
	},
	
	doStuff: function(){
		this.amount+=10;
		this.render();
		
	},

  render: function() {
	this.$el.html(this.template({}));//this.model.attributes));
	this.$el.find(".items").html("");
	var last_paid_at=null;
	for(var i = 0; i < this.model.length; ++i) {
			var m=this.model.at(i);
			if(last_paid_at!==m.get("paid_at")){
				this.$el.find(".items").append('<li data-role="list-divider">' + m.get("paid_at") + '</li>')
				last_paid_at=m.get("paid_at");
			}
			var m_expview = new ExpenseListItemView({model: m});
			this.$el.find(".items").append(m_expview.$el); 
			m_expview.render();
			if(i>=this.amount-1) break;
	}
	if(this.$el.find(".items").hasClass("ui-listview")){
		this.$el.find(".items").listview("refresh");
	} else {
		this.$el.find(".items").listview();
	}
	return this;
  }

});

