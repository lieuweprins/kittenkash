var CategoryView = Backbone.View.extend({

	el: "#category-view",

  initialize: function(options) {
	this.template = _.template($('#category-view-template').html());
	this.collection=options.collection;
    this.listenTo(this.model, "change", this.render);
    this.listenTo(this.collection, "change add remove update", this.render);
	this.amount=10;
	this.render();
  },
  
  events: {
		'click a.showmore':'doStuff'
	},
	
	doStuff: function(){
		this.amount+=10;
		this.render();
		
	},

  render: function() {
	  console.log("renderin'");
	this.$el.html(this.template(this.model.attributes));//));
	this.$el.find(".items").html("");
	var last_paid_at=null;
	for(var i = 0; i < this.collection.length; ++i) {
			var m=this.collection.at(i);
			if(m.get("category_id")==this.model.get("id")){
				if(last_paid_at!==m.get("paid_at")){
					this.$el.find(".items").append('<li data-role="list-divider">' + m.get("paid_at") + '</li>')
					last_paid_at=m.get("paid_at");
				}
				var m_expview = new ExpenseListItemView({model: m});
				this.$el.find(".items").append(m_expview.$el); 
				m_expview.render();
				if(i>=this.amount-1) break;
			}
	}
	/*if(this.$el.find(".items").hasClass("ui-listview")){
		this.$el.find(".items").listview("refresh");
	} else {
		this.$el.find(".items").listview();
	}*/
	$("#category-view").enhanceWithin();
	return this;
  }

});

