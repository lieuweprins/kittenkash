/*
	Lieuwaans database library voor gebruik in cordova met html5 fallback zodat het ook in de chrome browser werkt voor makkelijk testen. 
	usage:
	
	database
	
	//array of table create if not exists queries.
	
	var tableArray = ["CREATE TABLE IF NOT EXISTS stuff (`id` INTEGER, wortel, banaan, externe_pk INTEGER)"];
	
	database.init(tableArray, function(){
		alert("success, tables created!");
		
		//select query
		database.query("SELECT * FROM stuff WHERE id = ?", [5],  function(result){ console.log("success: " + result.rows.length + " rows returned.");console.log(result.rows.item(0)); }, function(error_message){ console.log("sql fout: " + error_message); }){
		
		//insert query getting key
		database.insert("INSERT INTO stuff (wortel, banaan) VALUES (?,?)", [22,55],  function(inserted_id){ console.log("success: row inserted as " + inserted_id + "."); }, function(error_message){ console.log("sql fout: " + error_message); }){
		
	}, function(error_message){
		alert("Database api niet geinitialiseerd: " + error_message);
	});
*/
var database = {
    
    shell: null,
	
	tables: [],
	
	_readyList: [],
	
	//execute when the plugin is ready.
	ready: function(query,params,onsuccess){
		if(database.shell!==null){
			database.query(query,params,onsuccess);
		} else {
			database._readyList.push({ "query" : query, "params" : params,"onsuccess": onsuccess });
		}
	},	
	
    init: function(tables, onsuccess,onerror){
		database.tables=tables;
        if(typeof(window.sqlitePlugin)!=="undefined"){
            //database.shell = window.sqlitePlugin.openDatabase("moneyfox.db", "1.0", "MONEYFOX", -1);
			database.shell = window.sqlitePlugin.openDatabase({name: "moneyfox.db", location: 2});

        } else {
			if(typeof(window.openDatabase)!=="undefined"){
            database.shell = window.openDatabase("moneyfox.db", "1.0", "MONEYFOX", 200000);
			} else {
				if(typeof(onerror)!=="undefined"){
					onerror("Platform does not support websql");
					return;
				}
			}
        }
        if(database.tables.length>0){ 
			database.populate(onsuccess,onerror);
		}
		$.each(database._readyList, function(i,item){
			database.query(item.query, item.params, item.onsuccess);
		});
		database._readyList=[];
 
    },
	
	getdate: function(){
		var date = new Date();date = date.getUTCFullYear() + '-' + ('00' + (date.getUTCMonth()+1)).slice(-2) + '-' + ('00' + date.getUTCDate()).slice(-2) + ' ' + ('00' + date.getUTCHours()).slice(-2) + ':' + ('00' + date.getUTCMinutes()).slice(-2) + ':' + ('00' + date.getUTCSeconds()).slice(-2);
		return date;
	},
	
	getlocaldate: function(){
		var date = new Date();date = date.getFullYear() + '-' + ('00' + (date.getMonth()+1)).slice(-2) + '-' + ('00' + date.getDate()).slice(-2) + ' ' + ('00' + date.getHours()).slice(-2) + ':' + ('00' + date.getMinutes()).slice(-2) + ':' + ('00' + date.getSeconds()).slice(-2);
		return date;
	},
    
     populate: function(onsuccess, onerror){
 
         async.eachSeries(database.tables,function(query,callback){
               database.query(query,[], function(){
                   callback();                              
                }, function(error){
                   callback(error);
                });
         }, function (err) {
             if(err==null){
				if(typeof(onsuccess)!=="undefined"){
					onsuccess();
				}
             } else {
				if(typeof(onerror)!=="undefined"){
					onerror();
				}
             }
        });
        
    },
            
    query: function(sql, params, onsuccess, onerror){        
        database.shell.transaction(function(tx){
            tx.executeSql(sql,params,function(tx,results){
                if(typeof(onsuccess)!=="undefined") onsuccess(results);
            }, function(tx,error){
                console.error(error.message);
           		if(typeof(onerror)!=="undefined") onerror(error.message);
            });
        });
    },
    
    insert: function(sql, params, onsuccess, onerror){
        //use query
        database.query(sql,params,function(results){
            if(typeof(onsuccess)!=="undefined"){
            	onsuccess(results.insertId);
            }
        },function(tx,error){
			if(typeof(onerror)!=="undefined"){
				onerror(tx,error);
			}
		});
    }
    
};